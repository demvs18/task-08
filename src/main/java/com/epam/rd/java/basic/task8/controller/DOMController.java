package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Main;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import static com.epam.rd.java.basic.task8.Main.FlowersXmlTags.*;


/**
 * Controller for DOM parser.
 */
public class DOMController extends Main {

    private final String xmlFileName;

    private final Set<Flower> flowers;
    private DocumentBuilder docBuilder;

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
        flowers = new HashSet<>();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            docBuilder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    // PLACE YOUR CODE HERE
    public Set<Flower> getFlowers() {
        return flowers;
    }

    public void buildSetFlowers() {
        Document doc;
        try {
            doc = docBuilder.parse(this.xmlFileName);
            Element root = doc.getDocumentElement();
            NodeList flowersList = root.getElementsByTagName(FLOWER.value());
            for (int i = 0; i < flowersList.getLength(); i++) {
                Element flowerElement = (Element) flowersList.item(i);
                Flower flower = buildFlower(flowerElement);
                flowers.add(flower);
            }
        } catch (IOException | SAXException e) {
            e.printStackTrace();
        }
    }

    private Flower buildFlower(Element flowerElement) {
        Flower flower = new Flower();

        flower.setName(getElementTextContext(flowerElement, NAME.value()));
        flower.setSoil(getElementTextContext(flowerElement, SOIL.value()));
        flower.setOrigin(getElementTextContext(flowerElement, ORIGIN.value()));

        VisualParameters visualParameters = flower.getVisualParameters();

        Element visualParametersElement =
                (Element) flowerElement.getElementsByTagName(VISUALPARAMETERS.value()).item(0);

        visualParameters.setStemColour(getElementTextContext(visualParametersElement, STEMCOLOUR.value()));
        visualParameters.setLeafColour(getElementTextContext(visualParametersElement, LEAFCOLOUR.value()));
        visualParameters.setAveLenFlower(Integer.parseInt(getElementTextContext(visualParametersElement, AVELENFLOWER.value())));

        flowerElement.getElementsByTagName("aveLenFlower").item(0).getAttributes().getNamedItem("measure").getTextContent();


        GrowingTips growingTips = flower.getGrowingTips();

        Element growingTipsElement =
                (Element) flowerElement.getElementsByTagName(GROWINGTIPS.value()).item(0);

        growingTips.setTempreture(Integer.parseInt(getElementTextContext(growingTipsElement, TEMPRETURE.value())));
        growingTips.setMeasureTempreture(flowerElement.getAttribute("measure"));
        growingTips.setLighting(flowerElement.getAttribute("lightRequiring"));
        growingTips.setWatering(Integer.parseInt(getElementTextContext(growingTipsElement, WATERING.value())));


        flower.setMultiplying(getElementTextContext(flowerElement, MULTIPLYING.value()));

        return flower;
    }

    private static String getElementTextContext(Element element, String elementName) {
        NodeList nList = element.getElementsByTagName(elementName);
        Node node = nList.item(0);
        return node.getTextContent();
    }




}
