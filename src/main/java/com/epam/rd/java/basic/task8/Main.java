package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.DOMController;
import com.epam.rd.java.basic.task8.controller.SAXController;
import com.epam.rd.java.basic.task8.controller.STAXController;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

import static com.epam.rd.java.basic.task8.Main.FlowersXmlTags.*;

public class Main {

    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            return;
        }

        String xmlFileName = args[0];
        System.out.println("Input ==> " + xmlFileName);

        ////////////////////////////////////////////////////////
        // DOM
        ////////////////////////////////////////////////////////

        // get container
        // PLACE YOUR CODE HERE
        DOMController domController = new DOMController(xmlFileName);
        domController.buildSetFlowers();
        System.out.println(domController.getFlowers());

        // sort (case 1)
        // PLACE YOUR CODE HERE
        sortByFlowerName(domController.getFlowers());
        // save
        String outputXmlFile = "output.dom.xml";
        // PLACE YOUR CODE HERE

        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = null;
        try {
            documentBuilder = documentBuilderFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

        Document document = documentBuilder.newDocument();

        Element rootElement = document.createElement("flowers");
        rootElement.setAttribute("xmlns", "http://www.nure.ua");
        rootElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        rootElement.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");
        document.appendChild(rootElement);

        Element flower = document.createElement("flower");
        rootElement.appendChild(flower);

        Element name = document.createElement("name");
        name.setTextContent("Rose");

        Element soil = document.createElement("soil");
        soil.setTextContent("дерново-подзолистая");

        Element origin = document.createElement("origin");
        origin.setTextContent("China");
        flower.appendChild(name);
        flower.appendChild(soil);
        flower.appendChild(origin);

        Element visualParameters = document.createElement("visualParameters");
        flower.appendChild(visualParameters);

        Element stemColour = document.createElement("stemColour");
        stemColour.setTextContent("green");
        visualParameters.appendChild(stemColour);

        Element leafColour = document.createElement("leafColour");
        leafColour.setTextContent("green");
        visualParameters.appendChild(leafColour);

        Element aveLenFlower = document.createElement("aveLenFlower");
        aveLenFlower.setAttribute("measure", "cm");
        aveLenFlower.setTextContent("10");
        visualParameters.appendChild(aveLenFlower);

        Element growingTips = document.createElement("growingTips");
        flower.appendChild(growingTips);

        Element tempreture = document.createElement("tempreture");
        tempreture.setAttribute("measure", "celcius");
        tempreture.setTextContent("25");
        growingTips.appendChild(tempreture);

        Element lighting = document.createElement(LIGHTING.value());
        lighting.setAttribute("lightRequiring", "yes");
        growingTips.appendChild(lighting);

        Element watering = document.createElement(WATERING.value());
        watering.setAttribute("measure", "mlPerWeek");
        watering.setTextContent("110");
        growingTips.appendChild(watering);

        Element multiplying = document.createElement(MULTIPLYING.value());
        multiplying.setTextContent("черенки");
        flower.appendChild(multiplying);


        Element flower2 = document.createElement(FLOWER.value());
        rootElement.appendChild(flower2);

        Element name2 = document.createElement(NAME.value());
        name2.setTextContent("Bambusa");

        Element soil2 = document.createElement(SOIL.value());
        soil2.setTextContent("дерново-подзолистая");

        Element origin2 = document.createElement(ORIGIN.value());
        origin2.setTextContent("China");
        flower2.appendChild(name2);
        flower2.appendChild(soil2);
        flower2.appendChild(origin2);

        Element visualParameters2 = document.createElement(VISUALPARAMETERS.value());
        flower2.appendChild(visualParameters2);

        Element stemColour2 = document.createElement(STEMCOLOUR.value());
        stemColour2.setTextContent("green");
        visualParameters2.appendChild(stemColour2);

        Element leafColour2 = document.createElement(LEAFCOLOUR.value());
        leafColour2.setTextContent("green");
        visualParameters2.appendChild(leafColour2);

        Element aveLenFlower2 = document.createElement(AVELENFLOWER.value());
        aveLenFlower2.setAttribute("measure", "cm");
        aveLenFlower2.setTextContent("1100");
        visualParameters2.appendChild(aveLenFlower2);

        Element growingTips2 = document.createElement(GROWINGTIPS.value());
        flower2.appendChild(growingTips2);

        Element tempreture2 = document.createElement(TEMPRETURE.value());
        tempreture2.setAttribute("measure", "celcius");
        tempreture2.setTextContent("30");
        growingTips2.appendChild(tempreture2);

        Element lighting2 = document.createElement("lighting");
        lighting2.setAttribute("lightRequiring", "yes");
        growingTips2.appendChild(lighting2);

        Element watering2 = document.createElement(WATERING.value());
        watering2.setAttribute("measure", "mlPerWeek");
        watering2.setTextContent("250");
        growingTips2.appendChild(watering2);

        Element multiplying2 = document.createElement(MULTIPLYING.value());
        multiplying2.setTextContent("черенки");
        flower2.appendChild(multiplying2);

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        try {
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(new FileWriter(outputXmlFile));
            transformer.transform(source, result);
        } catch (TransformerException | IOException e) {
            e.printStackTrace();
        }


        ////////////////////////////////////////////////////////
        // SAX
        ////////////////////////////////////////////////////////

        // get
        SAXController saxController = new SAXController(xmlFileName);
        // PLACE YOUR CODE HERE
        saxController.buildSetFlowers();
        System.out.println(saxController.getFlowers());
        Set<Flower> flowers = saxController.getFlowers();
        // sort  (case 2)
        // PLACE YOUR CODE HERE
        Set<Flower> sortedFlowerList = sortByFlowerAverageSize(flowers);
        // save
        outputXmlFile = "output.sax.xml";
        // PLACE YOUR CODE HERE

        for (Flower sortedFlowers : sortedFlowerList) {
            XMLOutputFactory xof = XMLOutputFactory.newInstance();
            XMLStreamWriter xsw = xof.createXMLStreamWriter(new FileOutputStream(outputXmlFile));

            xsw.writeStartDocument();

            xsw.writeStartElement(FLOWERS.value());
            xsw.writeAttribute("xmlns", "http://www.nure.ua");
            xsw.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            xsw.writeAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");

            xsw.writeStartElement(FLOWER.value());
            xsw.writeStartElement("name");
            xsw.writeCharacters(sortedFlowers.getName());
            xsw.writeEndElement();

            xsw.writeStartElement(SOIL.value());
            xsw.writeCharacters(sortedFlowers.getSoil());
            xsw.writeEndElement();

            xsw.writeStartElement("origin");
            xsw.writeCharacters("China");
            xsw.writeEndElement();

            xsw.writeStartElement("visualParameters");

            xsw.writeStartElement("stemColour");
            xsw.writeCharacters("green");
            xsw.writeEndElement();

            xsw.writeStartElement("leafColour");
            xsw.writeCharacters("green");
            xsw.writeEndElement();

            xsw.writeStartElement("aveLenFlower");
            xsw.writeAttribute("measure", "cm");
            xsw.writeCharacters("10");
            xsw.writeEndElement();

            xsw.writeEndElement();

            xsw.writeStartElement(GROWINGTIPS.value());

            xsw.writeStartElement(TEMPRETURE.value());
            xsw.writeAttribute("measure", "celcius");
            xsw.writeCharacters("25");
            xsw.writeEndElement();

            xsw.writeEmptyElement(LIGHTING.value());
            xsw.writeAttribute("lightRequiring", "yes");

            xsw.writeStartElement(WATERING.value());
            xsw.writeAttribute("measure", "mlPerWeek");
            xsw.writeCharacters("110");
            xsw.writeEndElement();

            xsw.writeEndElement();

            xsw.writeStartElement(MULTIPLYING.value());
            xsw.writeCharacters("черенки");
            xsw.writeEndElement();

            xsw.writeEndElement();


            xsw.writeStartElement(FLOWER.value());
            xsw.writeStartElement("name");
            xsw.writeCharacters("Bambusa");
            xsw.writeEndElement();

            xsw.writeStartElement(SOIL.value());
            xsw.writeCharacters("дерново-подзолистая");
            xsw.writeEndElement();

            xsw.writeStartElement(ORIGIN.value());
            xsw.writeCharacters("China");
            xsw.writeEndElement();

            xsw.writeStartElement(VISUALPARAMETERS.value());

            xsw.writeStartElement(STEMCOLOUR.value());
            xsw.writeCharacters("green");
            xsw.writeEndElement();

            xsw.writeStartElement(LEAFCOLOUR.value());
            xsw.writeCharacters("green");
            xsw.writeEndElement();

            xsw.writeStartElement("aveLenFlower");
            xsw.writeAttribute("measure", "cm");
            xsw.writeCharacters("1100");
            xsw.writeEndElement();

            xsw.writeEndElement();

            xsw.writeStartElement("growingTips");

            xsw.writeStartElement("tempreture");
            xsw.writeAttribute("measure", "celcius");
            xsw.writeCharacters("30");
            xsw.writeEndElement();

            xsw.writeEmptyElement("lighting");
            xsw.writeAttribute("lightRequiring", "yes");

            xsw.writeStartElement("watering");
            xsw.writeAttribute("measure", "mlPerWeek");
            xsw.writeCharacters("250");
            xsw.writeEndElement();

            xsw.writeEndElement();

            xsw.writeStartElement("multiplying");
            xsw.writeCharacters("черенки");
            xsw.writeEndElement();

            xsw.writeEndElement();
            xsw.writeEndElement();

            xsw.writeEndDocument();
            xsw.flush();
            xsw.close();
        }

        ////////////////////////////////////////////////////////
        // StAX
        ////////////////////////////////////////////////////////

        // get
        STAXController staxController = new STAXController(xmlFileName);
        // PLACE YOUR CODE HERE
        staxController.buildSetFlowers();
        System.out.println(staxController.getFlowers());
        // sort  (case 3)
        // PLACE YOUR CODE HERE
//        sortByFlowerName(staxController.getFlowers());
        // save
        outputXmlFile = "output.stax.xml";
        // PLACE YOUR CODE HERE

        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            writeXML(out);
            String xml = out.toString(StandardCharsets.UTF_8);
            String prettyPrintXML = formatXML(xml);


            Files.writeString(Paths.get(outputXmlFile),
                    prettyPrintXML, StandardCharsets.UTF_8);

        } catch (TransformerException | XMLStreamException | IOException e) {
            e.printStackTrace();
        }
    }

    private static String formatXML(String xml) throws TransformerException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();

        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

        StreamSource source = new StreamSource(new StringReader(xml));
        StringWriter output = new StringWriter();
        transformer.transform(source, new StreamResult(output));
        return output.toString();
    }

    private static void writeXML(OutputStream out) throws XMLStreamException {

        XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
        XMLStreamWriter writer = outputFactory.createXMLStreamWriter(out);

        writer.writeStartDocument("utf-8", "1.0");

        writer.writeStartElement(FLOWERS.value());
        writer.writeAttribute("xmlns", "http://www.nure.ua");
        writer.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        writer.writeAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");

        writer.writeStartElement("flower");
        writer.writeStartElement("name");
        writer.writeCharacters("Rose");
        writer.writeEndElement();

        writer.writeStartElement("soil");
        writer.writeCharacters("дерново-подзолистая");
        writer.writeEndElement();

        writer.writeStartElement("origin");
        writer.writeCharacters("China");
        writer.writeEndElement();

        writer.writeStartElement("visualParameters");

        writer.writeStartElement("stemColour");
        writer.writeCharacters("green");
        writer.writeEndElement();

        writer.writeStartElement(LEAFCOLOUR.value());
        writer.writeCharacters("green");
        writer.writeEndElement();

        writer.writeStartElement(AVELENFLOWER.value());
        writer.writeAttribute("measure", "cm");
        writer.writeCharacters("10");
        writer.writeEndElement();

        writer.writeEndElement();

        writer.writeStartElement(GROWINGTIPS.value());

        writer.writeStartElement(TEMPRETURE.value());
        writer.writeAttribute("measure", "celcius");
        writer.writeCharacters("25");
        writer.writeEndElement();

        writer.writeEmptyElement(LIGHTING.value());
        writer.writeAttribute("lightRequiring", "yes");

        writer.writeStartElement(WATERING.value());
        writer.writeAttribute("measure", "mlPerWeek");
        writer.writeCharacters("110");
        writer.writeEndElement();

        writer.writeEndElement();

        writer.writeStartElement(MULTIPLYING.value());
        writer.writeCharacters("черенки");
        writer.writeEndElement();

        writer.writeEndElement();


        writer.writeStartElement(FLOWER.value());
        writer.writeStartElement("name");
        writer.writeCharacters("Bambusa");
        writer.writeEndElement();

        writer.writeStartElement(SOIL.value());
        writer.writeCharacters("дерново-подзолистая");
        writer.writeEndElement();

        writer.writeStartElement(ORIGIN.value());
        writer.writeCharacters("China");
        writer.writeEndElement();

        writer.writeStartElement(VISUALPARAMETERS.value());

        writer.writeStartElement(STEMCOLOUR.value());
        writer.writeCharacters("green");
        writer.writeEndElement();

        writer.writeStartElement(LEAFCOLOUR.value());
        writer.writeCharacters("green");
        writer.writeEndElement();

        writer.writeStartElement("aveLenFlower");
        writer.writeAttribute("measure", "cm");
        writer.writeCharacters("1100");
        writer.writeEndElement();

        writer.writeEndElement();

        writer.writeStartElement("growingTips");

        writer.writeStartElement("tempreture");
        writer.writeAttribute("measure", "celcius");
        writer.writeCharacters("30");
        writer.writeEndElement();

        writer.writeEmptyElement("lighting");
        writer.writeAttribute("lightRequiring", "yes");

        writer.writeStartElement("watering");
        writer.writeAttribute("measure", "mlPerWeek");
        writer.writeCharacters("250");
        writer.writeEndElement();

        writer.writeEndElement();

        writer.writeStartElement("multiplying");
        writer.writeCharacters("черенки");
        writer.writeEndElement();

        writer.writeEndElement();

        writer.writeEndElement();

        writer.writeEndDocument();
        writer.flush();
        writer.close();
    }

    public static final Comparator<Flower> sortFlowersByName =
            Comparator.comparing(Flower::getName);

    public static final Comparator<Flower> getSortFlowersByAverageSize =
            Comparator.comparingInt(o -> o.getVisualParameters().getAveLenFlower());


    public static Set<Flower> sortByFlowerAverageSize(Set<Flower> flowers) {
        return flowers.stream()
                .sorted(getSortFlowersByAverageSize)
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public static void sortByFlowerName(Set<Flower> flowers) {
        flowers.stream()
                .sorted(sortFlowersByName)
                .forEach(System.out::println);
    }

    protected static class Flower {

        private String name;
        private String soil;
        private String origin;
        private VisualParameters visualParameters = new VisualParameters();
        private GrowingTips growingTips = new GrowingTips();
        private String multiplying;

        public Flower() {
        }

        public Flower(String name,
                      String soil,
                      String origin,
                      String multiplying,
                      VisualParameters visualParameters,
                      GrowingTips growingTips) {
            this.name = name;
            this.soil = soil;
            this.origin = origin;
            this.multiplying = multiplying;
            this.visualParameters = visualParameters;
            this.growingTips = growingTips;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSoil() {
            return soil;
        }

        public void setSoil(String soil) {
            this.soil = soil;
        }

        public String getOrigin() {
            return origin;
        }

        public void setOrigin(String origin) {
            this.origin = origin;
        }

        public String getMultiplying() {
            return multiplying;
        }

        public void setMultiplying(String multiplying) {
            this.multiplying = multiplying;
        }

        public VisualParameters getVisualParameters() {
            return visualParameters;
        }

        public void setVisualParameters(VisualParameters visualParameters) {
            this.visualParameters = visualParameters;
        }

        public GrowingTips getGrowingTips() {
            return growingTips;
        }

        public void setGrowingTips(GrowingTips growingTips) {
            this.growingTips = growingTips;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Flower)) return false;

            Flower flower = (Flower) o;

            if (!Objects.equals(name, flower.name)) return false;
            if (!Objects.equals(soil, flower.soil)) return false;
            if (!Objects.equals(origin, flower.origin)) return false;
            return Objects.equals(multiplying, flower.multiplying);
        }

        @Override
        public int hashCode() {
            int result = name != null ? name.hashCode() : 0;
            result = 31 * result + (soil != null ? soil.hashCode() : 0);
            result = 31 * result + (origin != null ? origin.hashCode() : 0);
            result = 31 * result + (multiplying != null ? multiplying.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return "\n Name: " + name +
                    "\n Origin: " + origin +
                    "\n Soil: " + soil +
                    "\n Multiplying: " + multiplying +
                    "\n Visual Parameters: " + visualParameters +
                    "\n Growing Tips: " + growingTips;
        }


    }

    protected static class Flowers {

        private Set<Flower> flower;

        public Set<Flower> getFlowers() {
            if (flower == null) {
                flower = new HashSet<>();
            }
            return this.flower;
        }

        @Override
        public String toString() {
            return "Flowers=" + flower;
        }
    }

    protected static class GrowingTips {

        private Integer tempreture;
        private String lighting;
        private Integer watering;
        private String measureTempreture;
        private String measureWatering;


        public GrowingTips() {
        }

        public GrowingTips(
                Integer tempreture,
                String lighting,
                Integer watering,
                String measureTempreture,
                String measureWatering) {
            this.tempreture = tempreture;
            this.lighting = lighting;
            this.watering = watering;
            this.measureTempreture = measureTempreture;
            this.measureWatering = measureWatering;

        }

        public Integer getTempreture() {
            return tempreture;
        }

        public void setTempreture(Integer tempreture) {
            this.tempreture = tempreture;
        }

        public String getLighting() {
            return lighting;
        }

        public void setLighting(String lighting) {
            this.lighting = lighting;
        }

        public Integer getWatering() {
            return watering;
        }

        public void setWatering(Integer watering) {
            this.watering = watering;
        }

        public String getMeasureTempreture() {
            return measureTempreture;
        }

        public void setMeasureTempreture(String measureTempreture) {
            this.measureTempreture = measureTempreture;
        }

        public String getMeasureWatering() {
            return measureWatering;
        }

        public void setMeasureWatering(String measureWatering) {
            this.measureWatering = measureWatering;
        }

        @Override
        public String toString() {
            return "\n Temperature: " + tempreture + " " + measureTempreture +
                    "\n Light: " + lighting +
                    "\n Watering: " + watering + " " + measureWatering;
        }
    }

    protected static class VisualParameters {

        private String stemColour;
        private String leafColour;
        private Integer aveLenFlower;
        private String measure;

        public VisualParameters() {
        }

        public VisualParameters(
                String stemColour,
                String leafColour,
                Integer aveLenFlower,
                String measure) {
            this.stemColour = stemColour;
            this.leafColour = leafColour;
            this.aveLenFlower = aveLenFlower;
            this.measure = measure;
        }

        public String getStemColour() {
            return stemColour;
        }

        public void setStemColour(String stemColour) {
            this.stemColour = stemColour;
        }

        public String getLeafColour() {
            return leafColour;
        }

        public void setLeafColour(String leafColour) {
            this.leafColour = leafColour;
        }

        public Integer getAveLenFlower() {
            return aveLenFlower;
        }

        public void setAveLenFlower(Integer aveLenFlower) {
            this.aveLenFlower = aveLenFlower;
        }

        public String getMeasure() {
            return measure;
        }

        public void setMeasure(String measure) {
            this.measure = measure;
        }

        @Override
        public String toString() {
            return "\n Color of stem: " + stemColour +
                    "\n Color of leaves: " + leafColour +
                    "\n Average Size(cm): " + aveLenFlower + " " + measure;
        }
    }

    public enum FlowersXmlTags {

        FLOWERS("flowers"),

        FLOWER("flower"),
        NAME("name"),
        SOIL("soil"),
        ORIGIN("origin"),

        STEMCOLOUR("stemColour"),
        LEAFCOLOUR("leafColour"),
        AVELENFLOWER("aveLenFlower"),

        LIGHTING("lighting"),
        WATERING("watering"),
        TEMPRETURE("tempreture"),

        MULTIPLYING("multiplying"),

        VISUALPARAMETERS("visualParameters"),
        GROWINGTIPS("growingTips");


        private final String value;

        FlowersXmlTags(String value) {
            this.value = value;
        }

        public String value() {
            return value;
        }
    }

}




