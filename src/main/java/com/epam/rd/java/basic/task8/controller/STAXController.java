package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Main;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Set;

import static com.epam.rd.java.basic.task8.Main.FlowersXmlTags.*;


/**
 * Controller for StAX parser.
 */
public class STAXController extends Main {

    private final String xmlFileName;
    private final Set<Flower> flowers;
    private XMLEventReader reader;

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
        flowers = new HashSet<>();
    }

    public Set<Flower> getFlowers() {
        return flowers;
    }

    // PLACE YOUR CODE HERE

    public void buildSetFlowers() {
        Flower flower = null;
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        try {
            reader = inputFactory.createXMLEventReader(new FileInputStream(xmlFileName));
            while (reader.hasNext()) {
                XMLEvent event = reader.nextEvent();

                if (event.isStartElement()) {

                    StartElement startElement = event.asStartElement();

                    if (startElement.getName().getLocalPart().equals("flower"))
                        flower = new Flower();
                    else if (startElement.getName().getLocalPart().equals(NAME.value())) {
                        event = reader.nextEvent();
                        if (flower != null) {
                            flower.setName(event.asCharacters().getData());
                        }
                    } else if (startElement.getName().getLocalPart().equals(SOIL.value())) {
                        event = reader.nextEvent();
                        if (flower != null) {
                            flower.setSoil(event.asCharacters().getData());
                        }
                    } else if (startElement.getName().getLocalPart().equals(ORIGIN.value())) {
                        event = reader.nextEvent();
                        if (flower != null) {
                            flower.setOrigin(event.asCharacters().getData());
                        }
                    } else if (startElement.getName().getLocalPart().equals(MULTIPLYING.value())) {
                        event = reader.nextEvent();
                        if (flower != null) {
                            flower.setMultiplying(event.asCharacters().getData());
                        }

                    } else if (event.isStartElement()) {
                        StartElement innerStartElement = event.asStartElement();
                        buildVisualParameters(innerStartElement, flower);
                        buildGrowingTips(innerStartElement, flower);
                    }
                }
                if (event.isEndElement()) {
                    EndElement endElement = event.asEndElement();
                    if (endElement.getName().getLocalPart().equals(FLOWER.value())) {
                        flowers.add(flower);
                    }
                }
            }
        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }
    }

    private void buildGrowingTips(StartElement innerStartElement, Flower flower) throws XMLStreamException {
        XMLEvent event;
        if (innerStartElement.getName().getLocalPart().equals(TEMPRETURE.value())) {
            event = reader.nextEvent();
            flower.getGrowingTips().setTempreture(Integer.parseInt(event.asCharacters().getData()));
            Attribute measureTempreture = innerStartElement.getAttributeByName(new QName("measure"));
            flower.getGrowingTips().setMeasureTempreture(measureTempreture.getValue());
        } else if (innerStartElement.getName().getLocalPart().equals(LIGHTING.value())) {
            Attribute lightRequiring = innerStartElement.getAttributeByName(new QName("lightRequiring"));
            flower.getGrowingTips().setLighting(lightRequiring.getValue());
        } else if (innerStartElement.getName().getLocalPart().equals(WATERING.value())) {
            event = reader.nextEvent();
            flower.getGrowingTips().setWatering(Integer.parseInt(event.asCharacters().getData()));
            Attribute measureWatering = innerStartElement.getAttributeByName(new QName("measure"));
            flower.getGrowingTips().setMeasureWatering(measureWatering.getValue());
        }
    }

    private void buildVisualParameters(StartElement innerStartElement, Flower flower) throws XMLStreamException {
        XMLEvent event;
        if (innerStartElement.getName().getLocalPart().equals(STEMCOLOUR.value())) {
            event = reader.nextEvent();
            flower.getVisualParameters().setStemColour(event.asCharacters().getData());
        } else if (innerStartElement.getName().getLocalPart().equals(LEAFCOLOUR.value())) {
            event = reader.nextEvent();
            flower.getVisualParameters().setLeafColour(event.asCharacters().getData());
        } else if (innerStartElement.getName().getLocalPart().equals(AVELENFLOWER.value())) {
            event = reader.nextEvent();
            flower.getVisualParameters().setAveLenFlower(Integer.valueOf(event.asCharacters().getData()));
            Attribute measure = innerStartElement.getAttributeByName(new QName("measure"));
            flower.getVisualParameters().setMeasure(measure.getValue());
        }
    }
}
