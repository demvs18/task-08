package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Main;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

/**
 * Controller for SAX parser.
 */
public class SAXController extends Main {

    private final String xmlFileName;
    private Set<Flower> flowers;
    private final FlowerHandler flowerHandler = new FlowerHandler();
    private XMLReader reader;

    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            SAXParser saxParser = factory.newSAXParser();
            reader = saxParser.getXMLReader();
        } catch (ParserConfigurationException | SAXException e) {
            e.printStackTrace();
        }
        reader.setContentHandler(flowerHandler);
    }

    public Set<Main.Flower> getFlowers() {
        return flowers;
    }

    public void buildSetFlowers() {
        try {
            reader.parse(xmlFileName);
        } catch (IOException | SAXException e) {
            e.printStackTrace();
        }
        flowers = flowerHandler.getFlowers();
    }


    // PLACE YOUR CODE HERE


    static class FlowerHandler extends DefaultHandler {

        private final Set<Main.Flower> flowers;
        private Main.Flower flower;
        private FlowersXmlTags currentTag;
        private final EnumSet<FlowersXmlTags> withText;
        private static final String FLOWER_ELEMENT = "flower";
        private final StringBuilder currentValue = new StringBuilder();

        public FlowerHandler() {
            flowers = new HashSet<>();
            withText = EnumSet.range(FlowersXmlTags.NAME, FlowersXmlTags.MULTIPLYING);
        }

        public Set<Main.Flower> getFlowers() {
            return flowers;
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) {

            currentValue.setLength(0);

            if (qName.equals("flower")) {
                flower = new Main.Flower();
            }

            if (qName.equals("aveLenFlower")) {
                String measure = attributes.getValue("measure");
                flower.getVisualParameters().setMeasure(measure);
            }


            if (qName.equals("lighting")) {
                String lighting = attributes.getValue("lightRequiring");
                flower.getGrowingTips().setLighting(lighting);
            }

            if (qName.equals("tempreture")) {
                String measureTempreture = attributes.getValue("measure");
                flower.getGrowingTips().setMeasureTempreture(measureTempreture);
            }

            if (qName.equals("watering")) {
                String wateringMeasure = attributes.getValue("measure");
                flower.getGrowingTips().setMeasureWatering(wateringMeasure);
            } else {
                FlowersXmlTags tmp = FlowersXmlTags.valueOf(qName.toUpperCase());
                if (withText.contains(tmp)) {
                    currentTag = tmp;
                }
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) {

            if (qName.equals("name")) {
                flower.setName(currentValue.toString());
            }
            if (qName.equals("soil")) {
                flower.setSoil(currentValue.toString());
            }
            if (qName.equals("origin")) {
                flower.setOrigin(currentValue.toString());
            }
            if (qName.equals("stemColour")) {
                flower.getVisualParameters().setStemColour(currentValue.toString());
            }
            if (qName.equals("leafColour")) {
                flower.getVisualParameters().setLeafColour(currentValue.toString());
            }
            if (qName.equals("aveLenFlower")) {
                flower.getVisualParameters().setAveLenFlower(Integer.valueOf(currentValue.toString()));
            }
            if (qName.equals("watering")) {
                flower.getGrowingTips().setWatering(Integer.valueOf(currentValue.toString()));
            }
            if (qName.equals("tempreture")) {
                flower.getGrowingTips().setTempreture(Integer.valueOf(currentValue.toString()));
            }
            if (qName.equals("multiplying")) {
                flower.setMultiplying(currentValue.toString());
            }
            if (qName.equals(FLOWER_ELEMENT)) {
                flowers.add(flower);
            }
        }

        @Override
        public void characters(char[] ch, int start, int length) {
            String data = new String(ch, start, length).strip();

            if (currentTag != null) {
                switch (currentTag) {
                    case NAME:
                        flower.setName(data);
                        break;
                    case SOIL:
                        flower.setSoil(data);
                        break;
                    case ORIGIN:
                        flower.setOrigin(data);
                        break;
                    case STEMCOLOUR:
                        flower.getVisualParameters().setStemColour(data);
                        break;
                    case LEAFCOLOUR:
                        flower.getVisualParameters().setLeafColour(data);
                        break;
                    case AVELENFLOWER:
                        flower.getVisualParameters().setAveLenFlower(Integer.parseInt(data));
                        break;
                    case TEMPRETURE:
                        flower.getGrowingTips().setTempreture(Integer.parseInt(data));
                        break;
                    case LIGHTING:
                        break;
                    case WATERING:
                        flower.getGrowingTips().setWatering(Integer.parseInt(data));
                        break;
                    case MULTIPLYING:
                        flower.setMultiplying(data);
                        break;
                    default:
                        throw new EnumConstantNotPresentException(currentTag.getDeclaringClass(), currentTag.name());
                }
            }
            currentTag = null;
            currentValue.append(ch, start, length);
        }
    }
}
